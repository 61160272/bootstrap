import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/forminput1',
    name: 'FormInput1',
    component: () => import(/* webpackChunkName: "about" */ '../views/FormInput1.vue')
  },
  {
    path: '/forminput2',
    name: 'FormInput2',
    component: () => import(/* webpackChunkName: "about" */ '../views/FormInput2.vue')
  },
  {
    path: '/forminput3',
    name: 'FormInput3',
    component: () => import(/* webpackChunkName: "about" */ '../views/FormInput3.vue')
  },
  {
    path: '/form1',
    name: 'Form1',
    component: () => import(/* webpackChunkName: "about" */ '../views/Form1.vue')
  },
  {
    path: '/form2',
    name: 'Form2',
    component: () => import(/* webpackChunkName: "about" */ '../views/Form2.vue')
  },
  {
    path: '/form3',
    name: 'Form3',
    component: () => import(/* webpackChunkName: "about" */ '../views/Form3.vue')
  },
  {
    path: '/form4',
    name: 'Form4',
    component: () => import(/* webpackChunkName: "about" */ '../views/Form4.vue')
  },
  {
    path: '/datatable1',
    name: 'Data Table 1',
    component: () => import(/* webpackChunkName: "about" */ '../views/DataTable1.vue')
  },
  {
    path: '/productform1',
    name: 'Product Form 1',
    component: () => import(/* webpackChunkName: "about" */ '../views/ProductForm1.vue')
  },
  {
    path: '/productTable',
    name: 'Product Form 1',
    component: () => import(/* webpackChunkName: "about" */ '../views/product/ProductTable.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
